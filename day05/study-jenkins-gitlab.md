### Jenkins + GitLab

> 小问题

```js
1.jenkins 服务器 任务打包好后 要首先 试试能否 ping 通nginx服务器
解决:
如果ping不通 查看网络设置模式 可以俩中都用桥接 这样内部虚拟机可互通访问
2. jenkins构建 dist 上传到nginx timeout
解决:
// 进入docker容器 手动联接一下 
docker exec -it jenkins  /bin/bash 
cd ~
scp demo.txt root@nginxIP:~
3. nginx authorized_keys
目的:主要是生成免密 密钥
```



##### 服务器之间通信

```js
服务器之间的通信:
1.jenkins 首先在docker容器里 生成公钥和私钥 
2.把自己的公钥配置到gitlab的web里，jenkins就可以拉/推代码了。
3.把jenkins的公钥配置到nginx服务器的auth..._keys中，就可以免密访问nginx了。
```

